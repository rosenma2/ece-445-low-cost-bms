#include <OneWire.h>
//byte addr[8];//onewire address
byte addr[8];
OneWire net(3);//declare one wire
void setup() {
  //set up one wire
  pinMode(3, INPUT_PULLUP);
  Serial.begin(9600);
  delay(1000);

  net.search(addr);
  for (int x = 0; x < 8; x++) {
    Serial.println(addr[x], HEX);
  }

}

void loop() {
  // put your main code here, to run repeatedly:
  net.reset();
  byte writebuf[3] = {0xCC, 0x69, 0x0E};
  byte readbuf[4] = {0xAA};
  net.write_bytes(writebuf, 3);
  net.read_bytes(readbuf, 4);
  int current =readbuf[0];
  current=current<<8;
  current+=readbuf[1];
  int Coulombs =readbuf[2];
  Coulombs=Coulombs<<8;
  Coulombs+=readbuf[3];
  Serial.print("Current: ");
  Serial.print(double(current)*0.0083333);
  Serial.println(" A");
  Serial.print("Coulombs: ");
  Serial.print(double(Coulombs)*0.0083333);
  Serial.println(" Ah");
  delay(1000);
}
