#define SCK_H 13
#define MOSI_H 11
#define MISO_H 12
#define SCK_L A1
#define MOSI_L A7
#define MISO_L A0
#define BALANCEBUTTON 5
#define DISCHARGEBUTTON 6
#define CHARGEBUTTON 7
#define DISCHARGERELAY 8
#define CHARGERELAY 9
#define ADC2VOLTAGECONSTANT 0.00442832
#define TEMPADCCONCSTANT 0.00200195503
#define THERMISTORNOMINAL 10000.0
#define SERIESRESISTOR 10000.0
#define BCOEFFICIENT 3435.0
#define SCHOTTKYDIODEDROP 0.3
#define VDIVIDERRESISTOR 10000.0
#define TEMPERATURENOMINAL 298.15
#define KELVIN2CELCIUS 273.15
#include <OneWire.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);
OneWire net(3);//declare one wire
String state;
long messageper= 0x3ff;
void setup() {
  // put your setup code here, to run once:v
  state = "Off";
  pinMode(MISO_L, OUTPUT);
  pinMode(SCK_L, INPUT);
  pinMode(MOSI_L, INPUT);

  pinMode(MISO_H, INPUT);
  pinMode(SCK_H, OUTPUT);
  pinMode(MOSI_H, OUTPUT);
  pinMode(BALANCEBUTTON, INPUT_PULLUP);
  pinMode(DISCHARGEBUTTON, INPUT_PULLUP);
  pinMode(CHARGEBUTTON, INPUT_PULLUP);
  pinMode(DISCHARGERELAY, OUTPUT);
  pinMode(CHARGERELAY, OUTPUT);
  // initialize the LCD
  lcd.begin();

  // Turn on the blacklight and print a message.
  lcd.backlight();
  Serial.begin(9600);
}

double bat1V;
double bat2V;
int lastbat1RAW = 0;
int lastbat2RAW = 0;
void loop() {
  // put your main code here, to run repeatedly:
  for (int wait=0; wait < 100; wait++) {
    if (!digitalRead(BALANCEBUTTON)) {
      digitalWrite(DISCHARGERELAY, LOW);
      digitalWrite(CHARGERELAY, LOW);
      state = "Balance";
      messageper= min(lastbat1RAW,lastbat2RAW);
    }
    if (!digitalRead(DISCHARGEBUTTON)) {
      digitalWrite(CHARGERELAY, LOW);
      digitalWrite(DISCHARGERELAY, HIGH);
      state = "Discharge";
      messageper=0x3FF;
    }
    if (!digitalRead(CHARGEBUTTON)) {
      digitalWrite(DISCHARGERELAY, LOW);
      digitalWrite(CHARGERELAY, HIGH);
      state = "Charge";
      messageper=0x3FF;
    }
    delay(10);
  }
  unsigned long messageout = messageper;
  unsigned long messagein1 = 0;
  unsigned long messagein2 = 0;
  byte writebuf[3] = {0xCC, 0x69, 0x0E};
  byte readbuf[4] = {0xAA};
  net.reset();
  net.write_bytes(writebuf, 3);
  net.read_bytes(readbuf, 4);
  int current = readbuf[0];
  current = current << 8;
  current += readbuf[1];
  int Coulombs = readbuf[2];
  Coulombs = Coulombs << 8;
  Coulombs += readbuf[3];
  //Serial.print("Current: ");
  //Serial.print(double(current) * 0.0083333);
  //Serial.println(" A");
  //Serial.print("Coulombs: ");
  //Serial.print(double(Coulombs) * 0.0083333);
  //Serial.println(" Ah");

  for (int x = 0; x < 40; x++) {
    digitalWrite(SCK_H, HIGH); // digitalWrite(3, LOW);
    digitalWrite (MOSI_H, (messageout & 0x80000000) >> 31); // digitalWrite(4, (messageout & 0x80000000) >> 31);
    messageout = messageout << 1;
    delayMicroseconds(50);
    digitalWrite(SCK_H, LOW); // digitalWrite(3, HIGH);
    if (x < 20)
    {
      messagein1 <<= 1;
      messagein1 |= (PINB & 0x10) >> 4;
    }
    else
    {
      messagein2 <<= 1;
      messagein2 |= (PINB & 0x10) >> 4;
    }
    delayMicroseconds(50);
    //delayMicroseconds(100);
  }

  digitalWrite(MOSI_H, LOW);
  //Serial.println("Message In: ");
  //Serial.println(((~messagein1) & 0x3FF)*ADC2VOLTAGECONSTANT);
  //Serial.println((~messagein1 >> 10) & 0x3FF);
  //Serial.println(((~messagein2) & 0x3FF)*ADC2VOLTAGECONSTANT);
  //Serial.println((~messagein2 >> 10) & 0x3FF);
  lastbat1RAW = ((~messagein1) & 0x3FF);
  bat1V = lastbat1RAW * ADC2VOLTAGECONSTANT;
  double bat1TV = ((~messagein1 >> 10) & 0x3FF) * TEMPADCCONCSTANT;
  double bat1T = bat1TV * VDIVIDERRESISTOR / (bat1V - SCHOTTKYDIODEDROP - bat1TV);
  bat1T = bat1T / VDIVIDERRESISTOR;
  bat1T = 1 / (log(bat1T) / BCOEFFICIENT + 1.0 / TEMPERATURENOMINAL) - KELVIN2CELCIUS;
  lastbat2RAW = ((~messagein2) & 0x3FF);
  bat2V = lastbat2RAW * ADC2VOLTAGECONSTANT;
  double bat2TV = ((~messagein2 >> 10) & 0x3FF) * TEMPADCCONCSTANT;
  double bat2T = bat2TV * VDIVIDERRESISTOR / (bat2V - SCHOTTKYDIODEDROP - bat2TV);
  bat2T = bat2T / VDIVIDERRESISTOR;
  bat2T = 1 / (log(bat2T) / BCOEFFICIENT + 1.0 / TEMPERATURENOMINAL) - KELVIN2CELCIUS;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Bat1: " + String(bat1V) + "V " + String(bat1T) + "C");
  lcd.setCursor(0, 1);
  lcd.print("Bat2: " + String(bat2V) + "V " + String(bat2T) + "C");
  lcd.setCursor(0, 2);
  lcd.print(String(double(current) * 0.0083333, 3) + "A " + String(double(Coulombs) * 0.0083333, 4) + "Ah");
  lcd.setCursor(0, 3);
  lcd.print(state + "TV: " + String(ADC2VOLTAGECONSTANT*messageper,3));

}
