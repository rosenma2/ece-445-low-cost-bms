void pciSetup(byte pin)
{
    *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
    PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
    PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}
volatile byte counter=0;
volatile bool messageReady = 0;
volatile int counterTimeOut=0;
volatile long messageStartTime = 0;
volatile long message =0;
volatile long messageout =0;
volatile unsigned long messageToSend = 0x1;
unsigned long data = 0x87654321;
int counterold =0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A0,OUTPUT);
  pinMode(A1,INPUT);
  pinMode(A7,INPUT);
  pciSetup(15);
  //Serial.println("SETUP DONE");
}

void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println(counter,HEX);
  if(millis()-messageStartTime>20)
    PORTC=0;
  if(messageReady){
    messageReady=0;
    Serial.print("Message:");
    Serial.println(messageout,HEX);
    Serial.println(counterold);
  }
}

ISR (PCINT1_vect) // handle pin change interrupt for D0 to D7 here
 {
     

     //Record Message 
     //Serial.println(PINC & 0x2);
     if(PINC & 0x2){
        message<<= 1;
        message|= (PINE & 0x8)>>3;
        counter++;
        //Serial.println(counter);
        //Check if full message recived
        if(counter ==32){
            //set message ready flag and ack on message
            messageReady=1;
            messageout = message;
            counterold =counter;
            counter=0;
            //messageToSend = 0x87654321;
        }
        else{
            //go to sleep
        }
     }
     else{
        //send output bit
        if(counter ==0){
           messageToSend = data;
           messageStartTime=millis();
           message=0;
        }
         //Check if message is stale
        else if(millis()-messageStartTime>50){
            counterold =counter;
            counter=0;
            //messageReady=1;
            message=0;
        }
        PORTC = (messageToSend & 0x80000000)>> 31;
        messageToSend= messageToSend<<1;
     }  
 }  
