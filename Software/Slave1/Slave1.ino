#define VLTG_ADC_PIN A3
#define TEMP_ADC_PIN A4
#define ADC_PWR A2
#define BLEED_PIN 5
#define SCK_H 13
#define MOSI_H 11
#define MISO_H 12
#define SCK_L A1
#define MOSI_L A7
#define MISO_L A0
#include <math.h>
volatile unsigned long target_voltage =0x3FF;
unsigned long messageout2per;
void pciSetup(byte pin)
{
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin)); // enable pin
  PCIFR |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

long temp_adc = 0;
long vltg_adc = 0;
int toggle = 0;

volatile byte counter = 0;
volatile bool messageReady = 0;
volatile int counterTimeOut = 0;
volatile unsigned long messageStartTime = 0;
volatile unsigned long message = 0;
volatile unsigned long messageout = 0;
volatile unsigned long messageToSend = 0x1;
volatile unsigned long messageToSend_next = 0x1;
unsigned long data = 0x87654321;
int counterold = 0;
volatile unsigned long messagein = 0;

ISR (PCINT1_vect) // handle pin change interrupt for D0 to D7 here
{
  //Record Message
  //Serial.println(PINC & 0x2);
  if (PINC & 0x2) {
    message <<= 1;
    message |= (PINE & 0x8) >> 3;
    counter++;
    //Serial.println(counter);
    //Check if full message recived
    if (counter == 40) {
      //set message ready flag and ack on message
      messageReady = 1;
      messageout = message;
      counterold = counter;
      counter = 0;
      temp_adc = analogRead(TEMP_ADC_PIN);
      vltg_adc = analogRead(VLTG_ADC_PIN);
      PORTC = 0x04;
      messageToSend = ((temp_adc << 10) | vltg_adc) << 12;
      //Serial.println(temp_adc);
      //Serial.println(vltg_adc);
      //Serial.print("M2S: ");
      //messageToSend=target_voltage<<12;
      //Serial.println(messageToSend, HEX);
      
      
      //messageToSend = 0x87654321;
    }
    else {
      //go to sleep
    }
  }
  else {
    //send output bit
    if (counter == 0) {
      messageStartTime = millis();
      message = 0;
    }
    if (counter == 20)
    {
      messageToSend_next = ~(messagein << 12);
    }
    //Check if message is stale
    else if (millis() - messageStartTime > 5000) {
      counterold = counter;
      counter = 0;
      //messageReady=1;
      message = 0;
    }
    if (counter < 20)
    {
      PORTC = 0x04|((messageToSend>> 31)&0x1);
      //Serial.println(messageToSend,HEX);
      //digitalWrite(3,(messageToSend >> 31)&0x1);
      messageToSend = messageToSend << 1;
    }
    else
    {
      PORTC = 0x04|((messageToSend_next & 0x80000000) >> 31);
      messageToSend_next = messageToSend_next << 1;
    }
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(MISO_L, OUTPUT);
  pinMode(SCK_L, INPUT);
  pinMode(MOSI_L, INPUT);

  pinMode(MISO_H, INPUT);
  pinMode(SCK_H, OUTPUT);
  pinMode(MOSI_H, OUTPUT);

  pciSetup(SCK_L);

  analogReference(EXTERNAL);
  pinMode(ADC_PWR, OUTPUT);
  digitalWrite(ADC_PWR, HIGH);

  //Balancing
  pinMode(BLEED_PIN, OUTPUT);
  digitalWrite(BLEED_PIN, LOW);

  temp_adc = analogRead(TEMP_ADC_PIN);
  vltg_adc = analogRead(VLTG_ADC_PIN);
  messageToSend = ~(((temp_adc << 10) | vltg_adc) << 12);
  Serial.println(messageToSend, HEX);
   pinMode(3, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(A2,HIGH);
  //Serial.print("Battery: ");
  //Serial.print(4.53017*analogRead(VLTG_ADC_PIN)/1023,4);
  //Serial.println("Temp: "+String(analogRead(TEMP_ADC_PIN)));
  /*
   * double(TEMP_ADC_PIN)/(2.212*VLTG_ADC_PIN)
   * 
   */
  unsigned long messageout2 = messageout2per;
  for (int x = 0; x < 20; x++) {
    digitalWrite(SCK_H, HIGH); // digitalWrite(3, LOW);
    digitalWrite (MOSI_H, (messageout2 & 0x80000000) >> 31); // digitalWrite(4, (messageout2 & 0x80000000) >> 31);
    delayMicroseconds(50);
    messageout2 = messageout2 << 1;
    digitalWrite(SCK_H, LOW); // digitalWrite(3, HIGH);
    messagein <<= 1;
    messagein |= (PINB & 0x10) >> 4;
    delayMicroseconds(50);
  }
  digitalWrite(MOSI_H, LOW);

  //Serial.print("Message In: ");
  //Serial.println((~messagein)&0x3FF);
  //Serial.println((~messagein>>10)&0x3FF);
  delay(1000);



  if (messageReady) {
    messageReady = 0;
    target_voltage = (~messageout>>8)&0x3FF;
    messageout2per = target_voltage<<12;
    Serial.println(target_voltage,HEX);
    Serial.println((~messageout),HEX);
    //Serial.println((~messageout>>10)&0x3FF);
    //Serial.println(counterold);
  }
  if(vltg_adc>target_voltage+3){
    digitalWrite(BLEED_PIN,HIGH);
  }
  else if(target_voltage>vltg_adc){
    digitalWrite(BLEED_PIN,LOW);
  }
}
