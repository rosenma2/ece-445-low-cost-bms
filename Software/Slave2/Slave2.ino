#define VLTG_ADC_PIN A3
#define TEMP_ADC_PIN A4
#define ADC_PWR A2
#define BLEED_PIN 5
#define SCK_H 13
#define MOSI_H 11
#define MISO_H 12
#define SCK_L A1
#define MOSI_L A7
#define MISO_L A0
#include <math.h>

void pciSetup(byte pin)
{
  *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin)); // enable pin
  PCIFR |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
  PCICR |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}

long temp_adc = 0;
long vltg_adc = 0;
int toggle = 0;
int balancetarget = 0x3FF;
volatile byte counter = 0;
volatile bool messageReady = 0;
volatile int counterTimeOut = 0;
volatile unsigned long messageStartTime = 0;
volatile unsigned long message = 0;
volatile unsigned long messageout = 0;
volatile unsigned long messageToSend = 0x1;
volatile unsigned long messageToSend_next = 0x1;
unsigned long data = 0x87654321;
int counterold = 0;
volatile unsigned long messagein = 0;

ISR (PCINT1_vect) // handle pin change interrupt for D0 to D7 here
{
  //Record Message
  //Serial.println(PINC & 0x2);
  if (PINC & 0x2) {
    message <<= 1;
    message |= (PINE & 0x8) >> 3;
    counter++;
    //Serial.println(counter);
    //Check if full message recived
    if (counter == 20) {
      //set message ready flag and ack on message
      messageReady = 1;
      messageout = message;
      counterold = counter;
      counter = 0;
      
      temp_adc = analogRead(TEMP_ADC_PIN);
      vltg_adc = analogRead(VLTG_ADC_PIN);
      PORTC = 0x04;
      messageToSend = ((temp_adc << 10) | vltg_adc) << 12;
      //Serial.println(temp_adc);
      //Serial.println(vltg_adc);
      //Serial.print("M2S: ");
      //messageToSend=0xAAAAAAAAA;
      //Serial.println(messageToSend, HEX);
      
      
      //messageToSend = 0x87654321;
    }
    else {
      //go to sleep
    }
  }
  else {
    //send output bit
    if (counter == 0) {
      messageStartTime = millis();
      message = 0;
    }
    //Check if message is stale
    else if (millis() - messageStartTime > 5000) {
      counterold = counter;
      counter = 0;
      //messageReady=1;
      message = 0;
    }
      PORTC = 0x04|((messageToSend>> 31)&0x1);
      messageToSend = messageToSend << 1;
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  pinMode(MISO_L, OUTPUT);
  pinMode(SCK_L, INPUT);
  pinMode(MOSI_L, INPUT);

  pinMode(MISO_H, INPUT);
  pinMode(SCK_H, OUTPUT);
  pinMode(MOSI_H, OUTPUT);

  pciSetup(SCK_L);

  analogReference(EXTERNAL);
  pinMode(ADC_PWR, OUTPUT);
  digitalWrite(ADC_PWR, HIGH);

  //Balancing
  pinMode(BLEED_PIN, OUTPUT);
  digitalWrite(BLEED_PIN, LOW);

  temp_adc = analogRead(TEMP_ADC_PIN);
  vltg_adc = analogRead(VLTG_ADC_PIN);
  messageToSend = ((temp_adc << 10) | vltg_adc) << 12;
  //Serial.println(messageToSend, HEX);
   pinMode(3, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(A2,HIGH);
  //Serial.print("Battery: ");
  //Serial.print(4.53017*analogRead(VLTG_ADC_PIN)/1023,4);
  //Serial.println("Temp: "+String(analogRead(TEMP_ADC_PIN)));
  /*
   * double(TEMP_ADC_PIN)/(2.212*VLTG_ADC_PIN)
   * 
   */
  delay(500);
  if(vltg_adc>balancetarget+3){
    digitalWrite(BLEED_PIN,HIGH);
  }
  else if(vltg_adc<balancetarget){
    digitalWrite(BLEED_PIN,LOW);
  }


  if (messageReady) {
    messageReady = 0;
    balancetarget = messageout&0x3FF;
    
  }
}

